import axios from 'axios';

const baseURL = process.env.REACT_APP_MOVIE_API;
const httpClient = axios.create({ baseURL });

export const getTrendingAnime = (page: number) => {
  return httpClient.get(`/top/anime/${page}/bypopularity`);
};
