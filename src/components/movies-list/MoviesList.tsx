import React, { useEffect, useState } from 'react';
import MovieItem from '../movie-item/MovieItem';
import Loader from '../loader/Loader';
import Styled from './MoviesList.styled';

import { getTrendingAnime } from '../../services/movie.service';
import { Anime } from '../../shared/movie.types';

interface Props {}

const MoviesList = (props: Props) => {
  const [movies, setMovies] = useState<Anime[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    getTrendingAnime(1)
      .then((res) => {
        const { data } = res;
        const newMovies = data.top.map((item: any): Anime => {
          const { mal_id, episodes, start_date, end_date, title, image_url, url, type, score } = item;

          return {
            id: mal_id,
            episodes,
            start_date,
            end_date,
            title,
            url,
            poster: image_url,
            score,
            type,
          };
        });

        setMovies(newMovies);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <Styled.Container>
      {movies.map((movie: Anime) => (
        <MovieItem key={movie.id} movie={movie} />
      ))}
    </Styled.Container>
  );
};

export default MoviesList;
