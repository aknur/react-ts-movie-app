import styled from 'styled-components';

const Container = styled.div`
  padding: 0 64px;
  display: flex;
  flex-wrap: wrap;
`;

const Styled = { Container };

export default Styled;
