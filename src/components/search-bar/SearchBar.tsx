import React, { useState } from 'react';
import { TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Styled from './SearchBar.styles';

interface Props {}

const SearchBar = (props: Props) => {
  const [searchText, setSearchText] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  return (
    <Styled.Container>
      <TextField
        fullWidth
        variant="outlined"
        color="primary"
        placeholder="Search..."
        value={searchText}
        onChange={handleChange}
        InputProps={{ endAdornment: <SearchIcon /> }}
      />
    </Styled.Container>
  );
};

export default SearchBar;
