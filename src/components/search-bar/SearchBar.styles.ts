import styled from 'styled-components';

const Container = styled.div`
  padding: 16px 64px;
`;

const Styled = { Container };

export default Styled;
