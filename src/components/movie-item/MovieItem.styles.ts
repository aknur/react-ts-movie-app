import styled from 'styled-components';

const Container = styled.div`
  width: 200px;
  padding: 8px;
  margin-bottom: 32px;
  position: relative;

  a {
    text-decoration: none;
    color: #000;
  }

  &:hover {
    a {
      color: #335969;
    }
  }
`;

const Score = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 40px;
  height: 25px;
  background-color: #4d94ae;
  color: #fff;
  text-align: center;
  line-height: 25px;
  z-index: 1;
`;

const PosterWrap = styled.div`
  width: 100%;
  height: 250px;
  margin-bottom: 16px;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    transform: scale(1.05);
    transition: 0.2s ease-out;
  }
`;

const Poster = styled.img`
  height: 100%;
`;

const Styled = { Container, Score, PosterWrap, Poster };
export default Styled;
