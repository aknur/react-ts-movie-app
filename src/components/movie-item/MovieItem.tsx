import React from 'react';
import { Typography } from '@material-ui/core';
import Styled from './MovieItem.styles';

import { Anime } from '../../shared/movie.types';

interface Props {
  movie: Anime;
}

const MovieItem = (props: Props) => {
  const { title, poster, score, url } = props.movie;

  return (
    <Styled.Container>
      <Styled.Score>{score}</Styled.Score>
      <a href={url} target="_blank">
        <Styled.PosterWrap>
          <Styled.Poster src={poster} />
        </Styled.PosterWrap>
        <Typography>{title}</Typography>
      </a>
    </Styled.Container>
  );
};

export default MovieItem;
