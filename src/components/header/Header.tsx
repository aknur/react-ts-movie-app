import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import Styled from './Header.styles';

interface Props {}

const navLinks = [
  { title: 'Popular', link: '/popular' },
  { title: 'Upcoming', link: '/upcoming' },
  { title: 'Random', link: '/random' },
];

const Header = (props: Props) => {
  return (
    <Styled.Wrap>
      <Link to="/">
        <Styled.Icon />
      </Link>
      <div>
        {navLinks.map((navLink) => (
          <Button component={Link} key={navLink.link} to={navLink.link}>
            {navLink.title}
          </Button>
        ))}
      </div>
    </Styled.Wrap>
  );
};

export default Header;
