import styled from 'styled-components';
import { ReactComponent as FujiIcon } from '../../assets/fuji.svg';

const Wrap = styled.header`
  padding: 32px 64px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Icon = styled(FujiIcon)`
  width: 40px;
`;

const Styled = { Wrap, Icon };

export default Styled;
