import styled from 'styled-components';

const Loader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const LoaderContainer = styled.div`
  width: 300px;
  height: 300px;
  display: flex;
  align-items: center;
`;

const Styled = {
  Loader,
  LoaderContainer,
};

export default Styled;
