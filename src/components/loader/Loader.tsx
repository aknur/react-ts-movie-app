import React from 'react';
import { Box, CircularProgress } from '@material-ui/core';
import Styled from './Loader.styles';

interface Props {
  size?: number;
}

const Loader = (props: Props) => {
  const { size } = props;

  return (
    <Styled.Loader>
      <Styled.LoaderContainer>
        <Box mx="auto" textAlign="center">
          <CircularProgress color="primary" size={size || 40} />
        </Box>
      </Styled.LoaderContainer>
    </Styled.Loader>
  );
};

export default Loader;
