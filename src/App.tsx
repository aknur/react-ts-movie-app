import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import SearchBar from './components/search-bar/SearchBar';
import Header from './components/header/Header';
import MoviesList from './components/movies-list/MoviesList';

import theme from './theme/theme';

function App() {
  return (
    <div>
      <Router>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Header />
          <SearchBar />
          <MoviesList />
        </ThemeProvider>
      </Router>
    </div>
  );
}

export default App;
