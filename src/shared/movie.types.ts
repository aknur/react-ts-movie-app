export enum AnimeType {
  'airing',
  'upcoming',
  'tv',
  'movie',
  'ova',
  'special',
}

export interface Anime {
  id: number;
  episodes: number;
  start_date: string;
  end_date: string;
  title: string;
  url: string;
  poster: string;
  score: number;
  type: AnimeType;
}
